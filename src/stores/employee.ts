import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/service/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useEmployeeStore = defineStore("employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const dialog = ref(false);
  const delDialig = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({
    name: " ",
    role: "",
    phone: "",
    salary: 0,
  });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedEmployee.value = { name: " ", role: "", phone: "", salary: 0 };
    }
  });
  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployees();
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to load resource.");
    }
    loadingStore.isLoading = false;
  }

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        await employeeService.saveEmployee(editedEmployee.value);
      }
      dialog.value = false;
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to save employee");
    }
    loadingStore.isLoading = false;
  }

  function editEmployee(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }

  async function deleteEmployee() {
    loadingStore.isLoading = true;
    try {
      await employeeService.deleteEmployee(delId);
      delId = -1;
      delDialig.value = false;
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to delete employee");
    }
    loadingStore.isLoading = false;
  }

  let delId = 0;
  function confirmDelete(id: number) {
    delDialig.value = true;
    delId = id;
  }

  return {
    employees,
    getEmployees,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    deleteEmployee,
    delDialig,
    confirmDelete,
  };
});
